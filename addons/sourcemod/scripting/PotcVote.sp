#include <cstrike>
#include <multicolors>
#include <sourcemod>
#include <sdkhooks>
#include <sdktools>

#pragma semicolon 1
#pragma newdecls required

public Plugin myinfo =
{
	name        = "PotcVoteSystem",
	author	    = "Neon, maxime1907, .Rushaway",
	description = "Vote system for Potc",
	version     = "1.2",
	url         = "https://steamcommunity.com/id/n3ontm"
}

#define NUMBEROFSTAGES 3

bool g_bVoteFinished = true;
bool g_bIsRevote = false;
bool bStartVoteNextRound = false;

static char g_sStageName[NUMBEROFSTAGES][512] = {"Classic", "Extreme", "Race Mode"};
int g_Winnerstage;

Handle g_VoteMenu = INVALID_HANDLE;
Handle g_StageList = INVALID_HANDLE;
Handle g_CountdownTimer = INVALID_HANDLE;

public void OnPluginStart()
{
	RegAdminCmd("sm_potcvote", Command_AdminStartVote, ADMFLAG_CONVARS, "sm_potcvote");

	RegServerCmd("sm_potcvote", Command_StartVote);
	RegServerCmd("sm_cancelcvote", Command_CancelVote);

	HookEvent("round_start",  OnRoundStart);
	HookEvent("round_end", OnRoundEnd);
}

public void OnMapStart()
{
	VerifyMap();

	PrecacheSound("#nide/Hoist The Colours - Potc.mp3", true);
	AddFileToDownloadsTable("sound/nide/Hoist The Colours - Potc.mp3");

	bStartVoteNextRound = false;
}

public Action VerifyMap()
{
	char currentMap[64];
	GetCurrentMap(currentMap, sizeof(currentMap));
	if (!StrEqual(currentMap, "ze_potc_v4s_4fix"))
	{
		char sFilename[256];
		GetPluginFilename(INVALID_HANDLE, sFilename, sizeof(sFilename));
		ServerCommand("sm plugins unload %s", sFilename);
	}
}

public void OnEntityCreated(int iEntity, const char[] sClassname)
{
	SDKHook(iEntity, SDKHook_SpawnPost, MyOnEntitySpawned);
}

public void MyOnEntitySpawned(int iEntity)
{
	if (g_bVoteFinished || !IsValidEntity(iEntity) || !IsValidEdict(iEntity))
		return;

	char sTargetname[128];
	GetEntPropString(iEntity, Prop_Data, "m_iName", sTargetname, sizeof(sTargetname));
	char sClassname[128];
	GetEdictClassname(iEntity, sClassname, sizeof(sClassname));

	if ((strcmp(sTargetname, "ext_bombsound2") != 0) && (strcmp(sTargetname, "ext_nukesound") != 0) && (strcmp(sClassname, "ambient_generic") == 0))
	{
		AcceptEntityInput(iEntity, "Kill");
	}
}

public void OnRoundEnd(Event hEvent, const char[] sEvent, bool bDontBroadcast)
{
	switch(GetEventInt(hEvent, "winner"))
	{
		case(CS_TEAM_CT):
		{
			int iCurrentStage = GetCurrentStage();
			
			if (iCurrentStage > -1)
				Cmd_StartVote();
		}
	}
}

public void OnRoundStart(Event hEvent, const char[] sEvent, bool bDontBroadcast)
{
	if (bStartVoteNextRound)
	{
		g_CountdownTimer = CreateTimer(1.0, StartVote, INVALID_HANDLE, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
		bStartVoteNextRound = false;
	}

	if (!(g_bVoteFinished))
	{
		int iCounter = FindEntityByTargetname(INVALID_ENT_REFERENCE, "Difficulty_Counter", "math_counter");
		if (iCounter != INVALID_ENT_REFERENCE)
			AcceptEntityInput(iCounter, "Kill");

		int iGameText = FindEntityByTargetname(INVALID_ENT_REFERENCE, "Level_Text", "game_text");
		if (iGameText != INVALID_ENT_REFERENCE)
			AcceptEntityInput(iGameText, "Kill");

		int iNewGameText;
		iNewGameText = CreateEntityByName("game_text");
		DispatchKeyValue(iNewGameText, "targetname", "intermission_game_text");
		DispatchKeyValue(iNewGameText, "channel", "4");
		DispatchKeyValue(iNewGameText, "spawnflags", "1");
		DispatchKeyValue(iNewGameText, "color", "255 128 0");
		DispatchKeyValue(iNewGameText, "color2", "255 255 0");
		DispatchKeyValue(iNewGameText, "fadein", "1");
		DispatchKeyValue(iNewGameText, "fadeout", "1");
		DispatchKeyValue(iNewGameText, "holdtime", "30");
		DispatchKeyValue(iNewGameText, "message", "“If you were waiting for the opportune moment, that was it.” -Jack Sparrow");
		DispatchKeyValue(iNewGameText, "x", "-1");
		DispatchKeyValue(iNewGameText, "y", ".01");
		DispatchKeyValue(iNewGameText, "OnUser1", "!self,Display,,0,-1");
		DispatchKeyValue(iNewGameText, "OnUser1", "!self,FireUser1,,5,-1");
		DispatchSpawn(iNewGameText);
		SetVariantString("!activator");
		AcceptEntityInput(iNewGameText, "FireUser1");

		int iMusic = FindEntityByTargetname(INVALID_ENT_REFERENCE, "ext_nukesound", "ambient_generic");
		if (iMusic != INVALID_ENT_REFERENCE)
		{
			SetVariantString("message #nide/Hoist The Colours - Potc.mp3");
			AcceptEntityInput(iMusic, "AddOutput");
			AcceptEntityInput(iMusic, "PlaySound");
		}
	}
}

public void GenerateArray()
{
	int iBlockSize = ByteCountToCells(PLATFORM_MAX_PATH);
	g_StageList = CreateArray(iBlockSize);

	for (int i = 0; i <= (NUMBEROFSTAGES - 1); i++)
		PushArrayString(g_StageList, g_sStageName[i]);

	int iArraySize = GetArraySize(g_StageList);

	for (int i = 0; i <= (iArraySize - 1); i++)
	{
		int iRandom = GetRandomInt(0, iArraySize - 1);
		char sTemp1[128];
		GetArrayString(g_StageList, iRandom, sTemp1, sizeof(sTemp1));
		char sTemp2[128];
		GetArrayString(g_StageList, i, sTemp2, sizeof(sTemp2));
		SetArrayString(g_StageList, i, sTemp1);
		SetArrayString(g_StageList, iRandom, sTemp2);
	}
}

public Action Command_AdminStartVote(int client, int argc)
{
	char name[64];
	int timer = 5;

	if (client == 0)
		name = "The server";
	else if(!GetClientName(client, name, sizeof(name))) 
		Format(name, sizeof(name), "Disconnected (uid:%d)", client);

	if (client != 0)
		CPrintToChatAll("{green}[SM] {cyan}%s {white}has initiated a potc vote round (In %d seconds)", name, timer);
	else
		CPrintToChatAll("{green}[SM] {cyan}%s {white}has initiated a potc vote round (Next round)", name, timer);	

	Cmd_StartVote();

	if (client != 0)
		ServerCommand("mp_restartgame %d", timer);

	return Plugin_Handled;
}

public Action Command_StartVote(int args)
{
	Cmd_StartVote();
	return Plugin_Handled;
}

public Action Command_CancelVote(int args)
{
	Cmd_CancelVote();
	return Plugin_Handled;
}

public void Cmd_StartVote()
{
	g_bVoteFinished = false;
	GenerateArray();
	bStartVoteNextRound = true;
}

public void Cmd_CancelVote()
{
	bStartVoteNextRound = false;
	CPrintToChatAll("{green}[PotcVote] {cyan}Zombies detected, aborting vote!");
}

public Action StartVote(Handle timer)
{
	static int iCountDown = 3;
	PrintCenterTextAll("[PotcVote] Starting Vote in %ds", iCountDown);

	if (iCountDown-- <= 0)
	{
		iCountDown = 5;
		KillTimer(g_CountdownTimer);
		g_CountdownTimer = INVALID_HANDLE;
		InitiateVote();
	}
}

public void InitiateVote()
{
	if(IsVoteInProgress())
	{
		CPrintToChatAll("{green}[PotcVote] {white}Another vote is currently in progress, retrying again in 5s.");
		g_CountdownTimer = CreateTimer(1.0, StartVote, INVALID_HANDLE, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
		return;
	}

	Handle menuStyle = GetMenuStyleHandle(view_as<MenuStyle>(0));
	g_VoteMenu = CreateMenuEx(menuStyle, Handler_PotcVoteMenu, MenuAction_End | MenuAction_Display | MenuAction_DisplayItem | MenuAction_VoteCancel);

	int iArraySize = GetArraySize(g_StageList);
	for (int i = 0; i <= (iArraySize - 1); i++)
	{
		char sBuffer[128];
		GetArrayString(g_StageList, i, sBuffer, sizeof(sBuffer));

		for (int j = 0; j <= (NUMBEROFSTAGES - 1); j++)
		{
			if (strcmp(sBuffer, g_sStageName[j]) == 0)
			{
					AddMenuItem(g_VoteMenu, sBuffer, sBuffer);
			}
		}
	}

	SetMenuOptionFlags(g_VoteMenu, MENUFLAG_BUTTON_NOVOTE);
	SetMenuTitle(g_VoteMenu, "What stage to play next?");
	SetVoteResultCallback(g_VoteMenu, Handler_SettingsVoteFinished);
	VoteMenuToAll(g_VoteMenu, 15);
}

public int Handler_PotcVoteMenu(Handle menu, MenuAction action, int param1, int param2)
{
	switch(action)
	{
		case MenuAction_End:
		{
			CloseHandle(menu);

			if (param1 != -1)
			{
				g_bVoteFinished = true;
				float fDelay = 3.0;
				CS_TerminateRound(fDelay, CSRoundEnd_GameStart, false);
			}
		}
	}
	return 0;
}

public int MenuHandler_NotifyPanel(Menu hMenu, MenuAction iAction, int iParam1, int iParam2)
{
	switch (iAction)
	{
		case MenuAction_Select, MenuAction_Cancel:
			delete hMenu;
	}
}

public void Handler_SettingsVoteFinished(Handle menu, int num_votes, int num_clients, const int[][] client_info, int num_items, const int[][] item_info)
{
	int highest_votes = item_info[0][VOTEINFO_ITEM_VOTES];
	int required_percent = 60;
	int required_votes = RoundToCeil(float(num_votes) * float(required_percent) / 100);

	if ((highest_votes < required_votes) && (!g_bIsRevote))
	{
		CPrintToChatAll("{green}[PotcVote] {white}A revote is needed!");
		char sFirst[128];
		char sSecond[128];
		GetMenuItem(menu, item_info[0][VOTEINFO_ITEM_INDEX], sFirst, sizeof(sFirst));
		GetMenuItem(menu, item_info[1][VOTEINFO_ITEM_INDEX], sSecond, sizeof(sSecond));
		ClearArray(g_StageList);
		PushArrayString(g_StageList, sFirst);
		PushArrayString(g_StageList, sSecond);
		g_bIsRevote = true;
		g_CountdownTimer = CreateTimer(1.0, StartVote, INVALID_HANDLE, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);

		return;
	}

	// No revote needed, continue as normal.
	g_bIsRevote = false;
	Handler_VoteFinishedGeneric(menu, num_votes, num_clients, client_info, num_items, item_info);
}

public void Handler_VoteFinishedGeneric(Handle menu, int num_votes, int num_clients, const int[][] client_info, int num_items, const int[][] item_info)
{
	g_bVoteFinished = true;
	char sWinner[128];
	GetMenuItem(menu, item_info[0][VOTEINFO_ITEM_INDEX], sWinner, sizeof(sWinner));
	float fPercentage = float(item_info[0][VOTEINFO_ITEM_VOTES] * 100) / float(num_votes);

	CPrintToChatAll("{green}[PotcVote] {white}Vote Finished! Winner: {red}%s{white} with %d%% of %d votes!", sWinner, RoundToFloor(fPercentage), num_votes);CPrintToChatAll("{green}[PotcVote] {white}Mooving to {red}%s{white}.", sWinner);

	for (int i = 0; i <= (NUMBEROFSTAGES - 1); i++)
	{
		if (strcmp(sWinner, g_sStageName[i]) == 0)
			g_Winnerstage = i;
	}

	ServerCommand("sm_stage %d", (g_Winnerstage + 1));

	float fDelay = 3.0;
	CS_TerminateRound(fDelay, CSRoundEnd_GameStart, false);
}

public int GetCurrentStage()
{
	int iLevelCounterEnt = FindEntityByTargetname(INVALID_ENT_REFERENCE, "Difficulty_Counter", "math_counter");

	int offset = FindDataMapInfo(iLevelCounterEnt, "m_OutValue");
	int iCounterVal = RoundFloat(GetEntDataFloat(iLevelCounterEnt, offset));

	int iCurrentStage;
	if (iCounterVal == 2)
		iCurrentStage = 1;
	else if (iCounterVal == 3)
		iCurrentStage = 2;
	else if (iCounterVal == 4)
		iCurrentStage = 3;
	else
		iCurrentStage = -1;

	return iCurrentStage;
}

public int FindEntityByTargetname(int entity, const char[] sTargetname, const char[] sClassname)
{
	if(sTargetname[0] == '#') // HammerID
	{
		int HammerID = StringToInt(sTargetname[1]);

		while((entity = FindEntityByClassname(entity, sClassname)) != INVALID_ENT_REFERENCE)
		{
			if(GetEntProp(entity, Prop_Data, "m_iHammerID") == HammerID)
				return entity;
		}
	}
	else // Targetname
	{
		int Wildcard = FindCharInString(sTargetname, '*');
		char sTargetnameBuf[64];

		while((entity = FindEntityByClassname(entity, sClassname)) != INVALID_ENT_REFERENCE)
		{
			if(GetEntPropString(entity, Prop_Data, "m_iName", sTargetnameBuf, sizeof(sTargetnameBuf)) <= 0)
				continue;

			if(strncmp(sTargetnameBuf, sTargetname, Wildcard) == 0)
				return entity;
		}
	}
	return INVALID_ENT_REFERENCE;
}
